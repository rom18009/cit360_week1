package collections;

public class Customer implements Comparable<Customer> {
    // Codewithmosh.com adaptation of HashSets and comparator interface
    private String name;

    public Customer(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(Customer other) {
        return name.compareTo(other.name);
    }

    @Override // override to return name
    public String toString() {
        return name;
    }
}
