package collections;

import com.sun.source.tree.Tree;

import java.util.*;
import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) {

// Codewithmosh.com adaptation of array lists
        System.out.println("****** ArrayList ******"); // ArrayList example
        String[] names = {"Max", "Dan", "Helen", "Doug", "Amber", "Steve"}; // arraylist
        System.out.println(names.length + " = Length of the Array"); // length of the array
        Arrays.sort(names); // sorts the names
        System.out.println(Arrays.toString(names)); //array contents to names

        for (Object str: names){
            System.out.println((String)str); // Sorted names list from the ArrayList
        }


        System.out.println(); // blank line
// Codewithmosh.com adaptation of HashSets and comparator interface
        System.out.println("****** Hash Set ******"); // Hash Set example
        Set<String> set = new HashSet<>();
        set.add("sky");
        set.add("blue");
        set.add("is");
        set.add("blue");
        System.out.println("This set has the duplicates removed but not sorted.");
        System.out.println(set); // prints the set, and removes the duplicates, but does not order the set

        System.out.println();
        System.out.println("This list is compared and sorted");  // compared to the other customer names and sorted using the Customer.java class
        List<Customer> customers = new ArrayList<>();
        customers.add(new Customer("Max"));
        customers.add(new Customer("Steve"));
        customers.add(new Customer("Dan"));
        customers.add(new Customer("Amber"));
        Collections.sort(customers);
        System.out.println(customers);


        System.out.println();
// Code taken and modified from https://www.javatpoint.com/java-treemap
        System.out.println("****** TreeMap ******"); // TreeMap Example
        TreeMap<Integer, String> map = new TreeMap<>();
            map.put(58, "Max");
            map.put(52, "Steve");
            map.put(59, "Amber");
            map.put(53, "Dan");

            for (Map.Entry m:map.entrySet()){
                System.out.println(m.getKey() + " " + m.getValue());
            }


        System.out.println();
// Codewithmosh.com adaptation of PriorityQueues
        System.out.println("****** Queue ******"); // Queue Example with exception on empty queue
        Queue<String> queue = new PriorityQueue<>();
        queue.add("Dan");
        queue.add("Max");
        queue.add("Amber");
        queue.add("Steve");

        System.out.println("The new queue contains " + queue);
        var first = queue.element(); // element to show first item in queue and return exception if empty
        System.out.println(first + " is first in the queue");
        var removeFirst = queue.remove();
        System.out.println(removeFirst + " was removed from the queue");
        System.out.println(queue);

        System.out.println();
        System.out.println("The new queue contains " + queue);
        var second = queue.element(); // element to show first item in queue and return exception if the queue is empty
        System.out.println(second + " is first in the queue");
        var removeSecond = queue.remove();
        System.out.println(removeSecond + " was removed from the queue");
        System.out.println(queue);

        System.out.println();
        System.out.println("The new queue contains " + queue);
        var third = queue.element();
        System.out.println(third + " is first in the queue");
        var removeThird = queue.remove();
        System.out.println(removeThird + " was removed from the queue");

        System.out.println();
        System.out.println("The new queue contains " + queue);
        var fourth = queue.element();
        System.out.println(fourth + " is first in the queue");
        var removeFourth = queue.remove();
        System.out.println(removeFourth + " was removed from the queue");

        System.out.println();
        System.out.println(queue + " this queue is now empty");
        queue.element(); // throw an exception if the queue is empty


    }
}

